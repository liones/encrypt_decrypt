// des.h
//
#ifndef __3DES_H__
#define __3DES_H__

#include "stdio.h"   
#include "memory.h"    
#include "stdlib.h"   
  
#define PLAIN_FILE_OPEN_ERROR 		-1   
#define KEY_FILE_OPEN_ERROR 		-2   
#define CIPHER_FILE_OPEN_ERROR 		-3   
#define DES_OK 						1  
#define BUFFER_SIZE 				1024
#define ROUND 						16//迭代次数，16次以上破解时只能使用穷举法

typedef char ElemType;  

   
//DES算法
int DES_Encrypt(ElemType *plainBuffer, ElemType *keyBuffer, ElemType *cipherBuffer, int n);//加密数据 
int DES_Decrypt(ElemType *cipherBuffer, ElemType *keyBuffer, ElemType *plainBuffer, int n);//解密数据 
int DES_Encrypt_File(char *plainFile, char *keyStr,char *cipherFile);//加密文件  
int DES_Decrypt_File(char *cipherFile, char *keyStr,char *plainFile);//解密文件 
//3DES算法
int D3DES_Encrypt(ElemType *plainBuffer, ElemType *keyBuffer, ElemType *cipherBuffer, int n);//加密数据 
int D3DES_Decrypt(ElemType *cipherBuffer, ElemType *keyBuffer, ElemType *plainBuffer, int n);//解密数据 
int D3DES_Encrypt_File(char *plainFile, char *keyStr,char *cipherFile);//加密文件  
int D3DES_Decrypt_File(char *cipherFile, char *keyStr,char *plainFile);//解密文件 

#endif