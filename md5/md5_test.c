#include <stdio.h>
#include <string.h>
#include "md5.h"

int main(int argc, char *argv[])    
{    
    MD5_CTX md5;  //定义一个MD5 text  
    MD5Init(&md5);//初始化             
    int i;    
    unsigned char encrypt[] ="admin";//要加密内容  
    //21232f297a57a5a743894a0e4a801fc3    
    unsigned char decrypt[16]; //加密结果       
    MD5Update(&md5,encrypt,strlen((char *)encrypt));//进行初步分组加密    
    MD5Final(&md5,decrypt);   //进行后序的补足，并加密   
      
    printf("加密前:%s\n加密后16位:",encrypt);    
    for(i=4;i<12;i++)    
    {    
        printf("%02x",decrypt[i]);  //02x前需要加上 %    
    }    
         
    printf("\n加密前:%s\n加密后32位:",encrypt);    
    for(i=0;i<16;i++)    
    {    
        printf("%02x",decrypt[i]);  //02x前需要加上 %    
    }
	printf("\n");

    return 0;    
}   