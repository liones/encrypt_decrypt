#ifndef __MD5_H__
#define __MD5_H__

  
//储存一个MD5 text信息   
typedef struct    
{    
    unsigned int count[2];      
    //记录当前状态，其数据位数     
      
    unsigned int state[4];      
    //4个数，一共32位 记录用于保存对512bits信息加密的中间结果或者最终结果    
      
    unsigned char buffer[64];  
    //一共64字节，512位        
}MD5_CTX;    


//函数声明区，每个函数在下面都有较详细说明，这里不再赘述  
  
void MD5Init(MD5_CTX *context);    
  
void MD5Update(MD5_CTX *context,unsigned char *input,unsigned int inputlen);    
  
void MD5Final(MD5_CTX *context,unsigned char digest[16]);    
  

#endif
