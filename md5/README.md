# md5
md5值计算，用于校验数据完整性

可以在linux上直接运行测试。
网页md5校验：http://tool.chinaz.com/tools/md5.aspx

linux下md5sum文件会在文件的最后增加一个'\n'，再进行md5运算
所以使用md5sum加密字符串的时候应该避免这个问题，要echo -n "passwd"|md5sum, -n代表不输出'\n'符。
